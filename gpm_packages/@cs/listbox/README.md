# README #

**listbox-extensions** is a library of functionality that extends or improves the LabVIEW listbox.

### How do I get set up?
- Install this package using [GPM](https://gpackage.io) and start coding
- All public API methods are clearly marked in the souce libraries (if you are calling non-API methods from souce, you're doing it wrong).

#### Examples
- No examples provided at this time

#### Dependencies
- This package depends only on base *vi.lib*

### Contribution guidelines
- All contributions must adhere to SOLID design principles.
- All code must be unit tested to the extent reasonably possible.
- Please contact the author if you want to contribute.

### Who do I talk to?
- Ethan Stern | Composed Systems, LLC
- ethan.stern@composed.io

### License
- See license file