# README #

**actor-framework-messages** is a set of message types for use with the LabVIEW Actor Framework. The messages included in this package solve common messaging problems with simple, robust, and testable messaging patterns. You can use these messages with any Actor types (no special inheritance necessary) letting you preserve a compositional application design.

There is nothing especially magical about these messages--they are sub-patterns of Command Pattern (i.e. Actor Framework) messaging that evolved from years of Actor Framework development. If you use and understand the standard Time-Delayed Message (and limitations thereof), then you might appreciate the functionality provided in this package.

# Message Types
## Message Cycle
This message type allows automatically cycling (repeating) a message but avoids the common problems with Time-Delayed Send Message, like:
- How do I execute cycles in quick succession while guaranteeing that I do not stack the Actor's queue with duplicate messages?
- How do I ensure the cycle stops instantaneously and executes a special cleanup message?
- How do I have the cycle timeout without having to poll Actor state and maintain a timestamp in class data?
- When I start the cycle, why does the first message not send immediately?

MessageCycle lets you send (and then automatically resend) a message to an Actor in a cyclic, but not phase-maintained, cycle. The cycle allows waits between iterations without blocking the Actor's thread while waiting. MessageCycle also allows timing out of the cycle with a pre-defined action upon timeout (programmer can inject a final timeout message to ensure the cycle has a determinable end). Similar to the Time-Delayed Send, starting a cycle returns a Notifier reference that allows external cancelation of the cycle.

## RoundTripMessage

This message type could be called many things, but its name is (*I think*) the most concise way to describe the behavior: I'm an Actor --> I send a RoundTripMessage to another Actor --> Upon execution, that message comes back to me. This message does not proxy another message--it makes the round trip as one message object--and it is the simplest (and inheritance-based) implementation of abstract messaging. To use it, you must implement your own Pack/Send method and also override *Do.vi* to invoke something concrete in one of the recipients. It might even be too simple to be widely useful. You decide!

### How do I get set up?
- Install this package using [GPM](https://gpackage.io) and start coding in the Actor Framework
- Some experience with AF messages is required to understand the patterns at first glance.

#### Examples
- No explicit examples provided at this time, but the automated tests located in the open-source repo (linked by GPM) illustrate basic use of the message types.

#### Dependencies
- This package depends only on NI's Actor Framework (in *vi.lib*)

### Contribution guidelines
- Please contact the author if you want to contribute.

### Who do I talk to?
- Ethan Stern | Composed Systems, LLC
- ethan.stern@composed.io

### License
- See license file